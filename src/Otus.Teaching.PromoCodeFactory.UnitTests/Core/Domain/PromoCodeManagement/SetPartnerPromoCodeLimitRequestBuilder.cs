﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement
{
    class SetPartnerPromoCodeLimitRequestBuilder 
    {
        private DateTime _endDate;
        private int _limit;

        public SetPartnerPromoCodeLimitRequestBuilder()
        {

        }

        public SetPartnerPromoCodeLimitRequestBuilder WithCreateEndDate(DateTime dateTime)
        {
            _endDate = dateTime;
            return this;
        }
        public SetPartnerPromoCodeLimitRequestBuilder WithCreateLimit(int limit)
        {
            _limit = limit;
            return this;
        }
        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = _endDate,
                Limit = _limit
            };
        }
    }
}
