﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement
{
    public class PartnerPromoCodeLimitBuilder
    {
        private Guid _id;
        private Guid _partnerId;
        private Partner _partner;
        private DateTime _createDate;
        private DateTime? _cancelDate;
        private DateTime _endDate;
        private int _limit;

        public PartnerPromoCodeLimitBuilder()
        {

        }

        public PartnerPromoCodeLimitBuilder WithCreateId(Guid id)
        {
            _id = id;
            return this;
        }
        public PartnerPromoCodeLimitBuilder WithCreatePartner(Partner partner)
        {
            _partnerId = partner.Id;
            _partner = partner;
            return this;
        }
        public PartnerPromoCodeLimitBuilder WithCreateCreateDate(DateTime dateTime)
        {
            _createDate = dateTime;
            return this;
        }
        public PartnerPromoCodeLimitBuilder WithCreateCancelDate(DateTime dateTime)
        {
            _cancelDate = dateTime;
            return this;
        }
        public PartnerPromoCodeLimitBuilder WithCreateEndDate(DateTime dateTime)
        {
            _endDate = dateTime;
            return this;
        }
        public PartnerPromoCodeLimitBuilder WithCreateLimit(int limit)
        {
            _limit = limit;
            return this;
        }
        public PartnerPromoCodeLimit Build()
        {
            return new PartnerPromoCodeLimit()
            {
                Id = _id,
                PartnerId = _partnerId,
                Partner = _partner,
                CreateDate = _createDate,
                CancelDate = _cancelDate,
                EndDate = _endDate,
                Limit = _limit
            };
        }
    }
}
