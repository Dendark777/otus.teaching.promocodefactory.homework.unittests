﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement
{
    public class PartnerBuilder
    {
        private Guid _id;
        private bool _isActive;
        private int _numberIssuedPromoCodes;
        private ICollection<PartnerPromoCodeLimit> _partnerLimits;

        public PartnerBuilder()
        {

        }

        public PartnerBuilder WithCreatedId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder WithCreateIsActive(bool isActive)
        {
            _isActive = isActive;
            return this;
        }

        public PartnerBuilder WithCreateNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }
        public PartnerBuilder WithCreateCollectionPartnerPromoCodeLimit(ICollection<PartnerPromoCodeLimit> partnerPromoCodeLimits)
        {
            _partnerLimits = partnerPromoCodeLimits;
            return this;
        }

        public Partner Builder()
        {
            return new Partner()
            {
                Id = _id,
                IsActive = _isActive,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                PartnerLimits = _partnerLimits
            };
        }


    }
}
