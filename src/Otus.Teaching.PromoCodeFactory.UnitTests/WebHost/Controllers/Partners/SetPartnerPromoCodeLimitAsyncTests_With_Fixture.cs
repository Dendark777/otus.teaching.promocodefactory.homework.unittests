﻿using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests_With_Fixture
    {
        public PartnersController controller;
        public Mock<IRepository<Partner>> _myMock;
        public Partner partner;
        public SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest;
        public Guid partnerId;
        public SetPartnerPromoCodeLimitAsyncTests_With_Fixture()
        {
            _myMock = new Mock<IRepository<Partner>>();
            _myMock.Setup(m => m.GetByIdAsync(partnerId))
                        .ReturnsAsync(partner);
            controller = new PartnersController(_myMock.Object);

            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(true)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(4)
                                                .Build();
        }
    }
}
