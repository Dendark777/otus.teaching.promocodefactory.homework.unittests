﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<SetPartnerPromoCodeLimitAsyncTests_With_Fixture>
    {
        //TODO: Add Unit Tests
        private PartnersController controller;
        private Mock<IRepository<Partner>> _myMock;
        private Partner partner;
        private SetPartnerPromoCodeLimitRequest setPartnerPromoCodeLimitRequest;
        private Guid partnerId;
        public SetPartnerPromoCodeLimitAsyncTests(SetPartnerPromoCodeLimitAsyncTests_With_Fixture myFixture)
        {
            _myMock = myFixture._myMock;
            controller = myFixture.controller;

            partnerId = myFixture.partnerId;
            partner = myFixture.partner;

            setPartnerPromoCodeLimitRequest = myFixture.setPartnerPromoCodeLimitRequest;
        }

        [Fact]//1
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Partner_Not_Found_Returns_404()
        {
            //Arrange
            partner = null;
            _myMock.Setup(m => m.GetByIdAsync(partnerId))
                        .ReturnsAsync(partner);
            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]//2
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Partner_Is_Not_Active_Returns_400()
        {
            //Arrange
            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(false)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(4)
                                                .Build();

            _myMock.Setup(m => m.GetByIdAsync(partnerId))
                                    .ReturnsAsync(partner);
            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        [Fact]//3 готово
        //Параметры
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Check_Reset_Limit_Promo_Code_Returns_True()
        {
            //Arrange
            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(true)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(4)
                                                .Build();
            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().Match(x => partner.NumberIssuedPromoCodes == 0);
        }

        [Fact]//4 Не готово
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Disabled_Befo_Limit_Returns_True()
        {
            //Arrange
            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                {
                    CancelDate = null,
                    EndDate = DateTime.Now.AddDays(-5)
                }
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(true)
                        .WithCreateNumberIssuedPromoCodes(10)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(40)
                                                .Build();
            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().Match(x => partner.NumberIssuedPromoCodes == 10);
        }
        [Fact]//5 Готово
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Limit_Less_Zero_Returns_True()
        {
            //Arrange
            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(true)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(4)
                                                .Build();
            setPartnerPromoCodeLimitRequest.Limit = -2;

            //Act
            IActionResult result =
                await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        [Fact]//6 готово
        public async Task PartnerControllerSetPartnerPromoCodeLimitAsync_Check_Save_Limit_To_Base_Returns_True()
        {
            //Arrange
            partnerId = new Guid("7D7E01BE-AADB-463E-BB0C-B4E03F3A2E56");
            var partnerPromoCodeLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
            };
            partner = new PartnerBuilder()
                        .WithCreatedId(partnerId)
                        .WithCreateIsActive(true)
                        .WithCreateCollectionPartnerPromoCodeLimit(partnerPromoCodeLimits)
                        .Builder();

            setPartnerPromoCodeLimitRequest = new SetPartnerPromoCodeLimitRequestBuilder()
                                                .WithCreateEndDate(DateTime.Now.AddDays(4))
                                                .WithCreateLimit(4)
                                                .Build();
            _myMock.Setup(m => m.GetByIdAsync(partnerId))
                                    .ReturnsAsync(partner);
            //Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

            //Assert
            _myMock.Verify(item => item.UpdateAsync(It.IsAny<Partner>()), Times.Once);
        }
    }
}